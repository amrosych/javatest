import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D://work/chromedriver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://stackoverflow.com/");
        WebElement search = driver.findElement(By.name("q"));
        search.sendKeys("webdriver");
        search.submit();

        driver.quit();
    }
}
